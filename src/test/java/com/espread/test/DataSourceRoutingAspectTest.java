package com.espread.test;

import com.espread.sys.service.SysResourceService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.espread.test.base.BaseTest;

/**
 * DataSourceRoutingAspectTest - Spring MVC测试框架详解——服务端测试
 */
public class DataSourceRoutingAspectTest extends BaseTest {

    @Autowired
    private SysResourceService sysResourceService;

    @Test
    public void testFindAllShop() {
        Integer count = sysResourceService.findAllResourceVo().size();
        System.out.println(count);
    }
}

